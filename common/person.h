/// @author Ivan Cukic for book "Functional Programming in C++"

#ifndef PERSON_H
#define PERSON_H

#include <iostream>
#include <string>

namespace {
class person_t {
public:
  enum gender_t { female, male, other };

  enum output_format_t { name_only, full_name };

  person_t() : m_name("John"), m_surname("Doe"), m_gender(other) {}

  person_t(std::string name, const std::string &surname, gender_t gender,
           int age = 0)
      : m_name(name), m_surname("Doe"), m_gender(gender), m_age(age),
        m_team_name("") {}

  person_t(std::string name, const std::string &surname, gender_t gender,
           std::string team_name, int age = 0)
      : m_name(name), m_surname(surname), m_gender(gender), m_age(age),
        m_team_name(team_name) {}

  std::string name() const { return m_name; }

  std::string surname() const { return m_surname; }

  gender_t gender() const { return m_gender; }

  int age() const { return m_age; }

  std::string team_name() const { return m_team_name; }

  void print(std::ostream &out, person_t::output_format_t format) const {
    if (format == person_t::name_only) {
      out << name() << std::endl;

    } else if (format == person_t::full_name) {
      out << name() << ' ' << surname() << std::endl;
    }
  }

private:
  std::string m_name;
  std::string m_surname;
  gender_t m_gender;
  int m_age;
  std::string m_team_name;
};

} // namespace

#endif // PERSON_H
