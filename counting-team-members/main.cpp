
#include "../common/person.h"

#include <algorithm>
#include <iostream>
#include <iterator>
#include <numeric>
#include <vector>

namespace {

class company_t {
public:
  company_t() {
    m_employees = {
        person_t("Peter", "Frampton", person_t::male, "apple", 62),
        person_t("Jane", "Doe", person_t::female, "apple", 62),
        person_t("Tom", "bombadil", person_t::male, "peach", 39),
        person_t("Martha", "Stewart", person_t::female, "peach", 38),
        person_t("Rose", "Davis", person_t::female, "peach", 33),
        person_t("David", "Copperfield", person_t::male, "olive", 32)};
  }

  std::string team_name_for(const person_t& employee) const;
  int count_team_members(const std::string& team_name) const;

private:
  std::vector<person_t> m_employees;
};

} // namespace

int main(int ac, char** av, char** en) {
  company_t company;
  std::cout << "apple has " << company.count_team_members("apple") << std::endl;
  std::cout << "peach has " << company.count_team_members("peach") << std::endl;
  std::cout << "olive has " << company.count_team_members("olive") << std::endl;
}

int company_t::count_team_members(const std::string& team_name) const {
  return std::count_if(m_employees.cbegin(), m_employees.cend(),
                       [this, &team_name](const person_t& employee) {
                         return team_name_for(employee) == team_name;
                       });
}

std::string company_t::team_name_for(const person_t& employee) const {
  return employee.team_name();
}