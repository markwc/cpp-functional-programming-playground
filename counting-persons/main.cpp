
#include "../common/person.h"

#include <algorithm>
#include <iostream>
#include <iterator>
#include <numeric>
#include <vector>

namespace {

class older_than {
public:
  older_than(int limit) : m_limit(limit) {
  }

  template <typename T> bool operator()(T&& object) const {
    return std::forward<T>(object).age() > m_limit;
  }

private:
  int m_limit{0};
};

} // namespace

int main(int ac, char** av, char** en) {
  std::vector<person_t> people = {
      person_t("Peter", "Frampton", person_t::male, 62),
      person_t("Jane", "Doe", person_t::female, 62),
      person_t("Tom", "bombadil", person_t::male, 39),
      person_t("Martha", "Stewart", person_t::female, 38),
      person_t("Rose", "Davis", person_t::female, 33),
      person_t("David", "Copperfield", person_t::male, 32)};
  auto older_than_40 =
      std::count_if(people.cbegin(), people.cend(), older_than(40));
  std::cout << "Found " << older_than_40 << " out of " << people.size()
            << " persons older than 40" << std::endl;

  auto older = std::vector<person_t>();
  std::copy_if(people.cbegin(), people.cend(), std::back_inserter(older),
               older_than(40));
  std::for_each(older.cbegin(), older.cend(), [](const person_t& person) {
    std::cout << person.name() << " is older than 40" << std::endl;
  });

  auto females = std::vector<person_t>();
  std::copy_if(people.cbegin(), people.cend(), std::back_inserter(females),
               [](const person_t& person) {
                 return person_t::female == person.gender();
               });
  std::for_each(females.cbegin(), females.cend(), [](const person_t& person) {
    std::cout << person.name() << " is female" << std::endl;
  });

  auto female_list = std::accumulate(
      std::next(females.cbegin()), females.cend(), females.cbegin()->name(),
      [](std::string list, const person_t& person) {
        return list + ", " + person.name();
      });
  std::cout << "Females: " << female_list << std::endl;

  auto average = std::accumulate(females.cbegin(), females.cend(), 0,
                                 [](int b, const person_t& person) {
                                   return person.age() + b;
                                 }) /
                 females.size();
  std::cout << average << " average life for females" << std::endl;
}